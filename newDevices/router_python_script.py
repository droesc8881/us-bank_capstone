#Router python script file

import serial
import time
import sys

def main():
    #test connection to device
    try:
        console = serial.Serial(
            port='COM3', #Change COM# as needed
            baudrate=9600,
            parity="N",
            stopbits=1,
            bytesize=8,
            timeout=8
        )
    #If there is an exception, print out the error and exit the program
    except serial.serialutil.SerialException as e:
        print('Error connecting to device...', e)
        sys.exit()
    #if there is no exception, run the commands and print the output.
    else:
        #command ='\r\n\nen\nshow clock\n'.encode('utf-8') <--- This is an example of what the function below is based on
        
        command ='\r\n\nen\nconf t\nhostname US_BANK_Router\nip domain name usbank.com\nline vty 0 4\nlogin local\ntransport input ssh\nexit\ncrypto key generate rsa modulus 2048 general-keys\nip ssh version 2\nusername cisco privilege 15 password cisco\nint gigabitethernet 0/0/0\nip address 192.168.64.5 255.255.255.252\nno shutdown\nip routing\nip route 0.0.0.0 0.0.0.0 192.168.64.6\n'.encode('utf-8')
        #This command will enable ssh on the router plus add an interface + ip address 
        #int gigabitethernet 
        #192.168.99.253 255.255.255.0
        console.write(command)
        time.sleep(3)

        output = console.read(225).decode('utf-8')
        print(output)
if __name__ == "__main__":
    main()